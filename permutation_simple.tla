---------------------------- MODULE permutation ----------------------------

EXTENDS Naturals, TLAPS
CONSTANTS L, BYTES, pA, pB, mem0, pm

Number == Nat \ {0} 

ASSUME ConstAssumption == 
       /\ pA \in Nat 
       /\ pB \in Nat
       /\ pB > pA+L
       /\ L \in Nat \ {0}
       /\ mem0 \in [Nat -> BYTES]
       /\ pm \in Nat
       
VARIABLES eip, al, ah, ecx, mem
  
MsA(x) == \A i \in (0..x-1) : mem[pA+i] = mem0[pB+i]
MsB(x) == \A i \in (0..x-1) : mem[pB+i] = mem0[pA+i]
MB(x) == \A i \in (x..L-1) : mem[pB+i] = mem0[pB+i]
MA(x) == \A i \in (x..L-1) : mem[pA+i] = mem0[pA+i]
  
Init == 
    /\ ecx=0
    /\ eip = pm
    /\ mem = mem0
    /\ ah \in BYTES
    /\ al \in BYTES

I0 == 
   /\ eip = pm
   /\ eip' = eip+6
   /\ al' = mem[pA+ecx]
   /\ UNCHANGED <<ah, ecx, mem>>

I1 == 
   /\ eip = pm+6
   /\ eip' = eip+6
   /\ ah' = mem[pB+ecx]
   /\ UNCHANGED <<al, ecx, mem>>

I2 == 
   /\ eip = pm+12
   /\ eip' = eip+6
   /\ mem' = [mem EXCEPT ![pA+ecx] = ah]
   /\ UNCHANGED <<al, ah, ecx>>

I3 == 
    /\ eip = pm+18
    /\ eip' = eip+1
    /\ mem' = [mem EXCEPT ![pB+ecx] = al]
    /\ UNCHANGED <<al, ah, ecx>>
  
I4 == 
    /\ eip = pm+19
    /\ eip = pm
    /\ ecx' = ecx + 7
    /\ UNCHANGED <<al, ah,  mem>>
   
I5 == 
    \/ /\ eip = pm+26
       /\ ecx = L
       /\ eip' = pm+27
       /\ UNCHANGED <<al, ah, ecx,  mem>>
    \/ /\ eip = pm+26
       /\ ecx < L
       /\ eip' = pm
       /\ UNCHANGED <<al, ah, ecx,  mem>>       
                  
Next ==
\/ I0
\/ I1
\/ I2
\/ I3
\/ I4
\/ I5

TypeInv == /\ ecx \in (0..L) 
           /\ mem \in [Nat -> BYTES]
           /\ ah \in BYTES
           /\ al \in BYTES
           
Phi0 == /\ TypeInv
        /\ ecx \in (0..L-1)
        /\ MsA(ecx) 
        /\ MsB(ecx) 
        /\ MA(ecx) 
        /\ MB(ecx)
 
Phi1 == /\ Phi0
        /\ al = mem0[pA+ecx]   
             
Phi2 == /\ Phi1
        /\ ah = mem0[pB+ecx]    
           
Phi3 == /\ TypeInv
        /\ ecx \in (0..L-1) 
        /\ MsA(ecx+1) 
        /\ MsB(ecx) 
        /\ MA(ecx+1) 
        /\ MB(ecx)
        /\ al = mem0[pA+ecx]
        
Phi4 == /\ ecx \in (0..L-1) 
        /\ TypeInv
        /\ MsA(ecx+1) 
        /\ MsB(ecx+1) 
        /\ MA(ecx+1) 
        /\ MB(ecx+1)
        
Phi5 == /\ TypeInv
        /\ MsA(ecx) 
        /\ MsB(ecx) 
        /\ MA(ecx) 
        /\ MB(ecx)   
        
Phi7 == /\ TypeInv
        /\ MsA(L)
        /\ MsB(L)           

PermuteInvariant == \/ /\ eip = pm
                       /\ Phi0
                    \/ /\ eip = pm+6
                       /\ Phi1
                    \/ /\ eip = pm+12
                       /\ Phi2
                    \/ /\ eip = pm+18
                       /\ Phi3
                    \/ /\ eip = pm+19
                       /\ Phi4                                                     
                    \/ /\ eip = pm+26
                       /\ Phi5
                    \/ /\ eip = pm+27
                       /\ Phi7     
                   
LEMMA SubInterval == TypeInv => \A i \in (0..(ecx+1)-1) : i \in (0..ecx-1) \/ i = ecx
  BY ConstAssumption DEF TypeInv

LEMMA L1 == TypeInv => (0..(ecx+1)-1) = (0..ecx)
        BY ConstAssumption DEF TypeInv        
  
LEMMA ConstDomainB == ASSUME
                      /\ ecx \in (0..L-1)
                      /\ mem' = [mem EXCEPT ![pB+ecx] = al] 
                      /\ mem \in [Nat ->BYTES] 
                  PROVE \A i \in (0..ecx-1) : mem'[pB+i] = mem[pB+i]
      <1>1 \A i \in DOMAIN mem \ {pB+ecx} : mem'[i] = mem[i]
      OBVIOUS            
      <1>2 pB+ecx <= pB+L-1 /\ pB <= pB /\ pB+ecx > pB+ecx-1
      BY ConstAssumption
      <1>0 QED BY ConstAssumption, <1>1, <1>2               
  
LEMMA InvClosureB == ASSUME
        ecx \in (0..L-1), ecx'=ecx,
        mem \in [Nat-> BYTES],
        mem' = [mem EXCEPT ![pB+ecx] = al],
        MB(ecx)
       PROVE MB(ecx+1)'
      <1> SUFFICES \A i \in (ecx+1..L-1) : mem'[pB+i] = mem0[pB+i]
      BY ConstAssumption DEF MB
      <1>1 \A i \in (ecx+1..L-1) : mem'[pB+i] = mem[pB+i]
        BY mem' = [mem EXCEPT ![pB+ecx] = al] /\ mem \in  [pB..pB+L-1 -> BYTES], ConstAssumption      
      <1>2 \A i \in (ecx+1..L-1) : mem[pB+i] = mem0[pB+i]
        BY DEF MB
      <1>0. QED BY <1>1, <1>2, ConstAssumption DEF MB   
                      
LEMMA InvClosureA == ASSUME 
        ecx \in (0..L-1), 
        ecx'=ecx, 
        mem \in [Nat -> BYTES], 
        mem' = [mem EXCEPT ![pB+ecx] = al], 
        MsB(ecx)
     PROVE 
      MsB(ecx)'
  <1> USE DEF MsB    
  <1> SUFFICES \A i \in (0..ecx-1) : mem'[pB+i] = mem0[pA+i]   
  OBVIOUS
  <1>1 \A i \in (0..ecx-1) : mem'[pB+i] = mem[pB+i]
  BY mem' = [mem EXCEPT ![pB+ecx] = al] /\ mem \in  [pB..pB+L-1 -> BYTES], ConstAssumption
  <1>0. QED BY <1>1, ConstAssumption DEF MsB
  
 
LEMMA ExtendSub == ASSUME 
                ecx \in (0..L-1), 
                MsB(ecx)', 
                mem'[pB+ecx] = mem0[pA+ecx], ecx' = ecx 
                   PROVE MsB(ecx+1)'              
     <1>1 SUFFICES /\ \A i \in (0..ecx-1) : mem'[pB+i] = mem0[pA+i]
                   /\ mem'[pB+ecx] = mem0[pA+ecx]
                   BY DEF MsB
     <1>0. QED BY ConstAssumption, <1>1 DEF MsB

                                     
LEMMA F0 == Phi0 /\ I0 => Phi1' 
  <1>1. Phi0 /\ I0 => Phi0'  
    <2>0. QED BY ConstAssumption DEF Phi0, I0, MA, MB, MsA, MsB, TypeInv
  <1>2. Phi0 /\ I0 => al' = mem0[pA+ecx]
  BY DEF Phi0, I0, MA
  <1>0. QED BY ConstAssumption, <1>1, <1>2 DEF Phi0, Phi1, I0
  
LEMMA F1 == Phi1 /\ I1 => Phi2'
  <1> SUFFICES ASSUME Phi1, I1 PROVE Phi2' OBVIOUS
  <1> SUFFICES Phi0'
               /\ al' = mem0[pA+ecx]
               /\ ah' = mem0[pB+ecx]
               BY DEF Phi1, Phi0, Phi2, I1   
  <1> USE DEF Phi1, Phi0, TypeInv
  <1>1 Phi0'
  BY ConstAssumption DEF Phi1, Phi0, I1, MA, MB, MsA, MsB
  <1>2. al' = mem0[pA+ecx] /\ ah'=mem0[pB+ecx]
  BY DEF Phi0, Phi1, I1, MB
  <1>0. QED BY ConstAssumption, <1>1, <1>2
 
LEMMA F2 == Phi2 /\ I2 => Phi3'
    <1> SUFFICES ASSUME Phi2,I2 PROVE TypeInv' /\ ecx' \in (0..L-1) /\ MB(ecx)' /\ MsB(ecx)' /\ 
                                       MA(ecx+1)' /\ MsA(ecx+1)' /\ al' = mem0[pA+ecx]
                   BY DEF Phi3, TypeInv, Phi2, Phi1, Phi0, I2
   <1> USE DEF Phi2, Phi1, Phi0, TypeInv, Phi3, I2
   <1>1. TypeInv'
   BY ConstAssumption DEF Phi0, Phi1, Phi2, TypeInv, I2
   <1>2. ecx' \in (0..L-1)
   BY DEF Phi0, Phi1, Phi2, I2
   <1>3. MB(ecx)' /\ MsB(ecx)'
      <2> SUFFICES /\ \A i \in (0..ecx-1) : mem'[pB+i] = mem0[pA+i] 
                   /\ \A i \in (ecx..L-1) : mem'[pB+i] = mem0[pB+i] 
           BY ConstAssumption DEF MB, MsB
      <2>1 MB(ecx) /\ MsB(ecx) BY DEF MB, MsB
      <2>3 \A i \in (0..L-1) : mem[pB+i] = mem'[pB+i] BY I2, ConstAssumption
      <2>4 \A i \in (0..ecx-1) : mem'[pB+i] = mem0[pA+i] 
        BY <2>1, <2>3, ConstAssumption, I2, Phi2, TypeInv, (0..ecx-1) \subseteq (0..L-1) DEF Phi1, Phi0, MsB
      <2>5 \A i \in (ecx..L-1) : mem'[pB+i] = mem0[pB+i] 
       <3>1 \A i \in (ecx..L-1) : mem[pB+i] = mem0[pB+i]
           BY <2>1, I2, ConstAssumption DEF MB
       <3>2 \A i \in (ecx..L-1) : mem'[pB+i] = mem[pB+i]
           BY <2>3, (ecx..L-1) \subseteq (0..L-1)    
       <3>0 QED BY <3>1, <3>2, ConstAssumption
      <2>0 QED  BY ConstAssumption,<2>1, <2>3, <2>4, <2>5 DEF MB, MsB, Phi0, I2
   <1>4. MA(ecx+1)'
      <2>1 mem' = [mem EXCEPT ![pA+ecx] = ah]
         BY DEF Phi2, Phi1, Phi0, I2
      <2>2 \A i \in (ecx+1..L-1) : mem[pA+i] = mem0[pA+i]
          BY DEF Phi2, Phi1, Phi0, MA
      <2>3 ecx' = ecx
      BY DEF Phi2, Phi1, Phi0, MA, I2   
      <2>4 \A i \in (ecx+1..L-1) : mem[pA+i] = mem'[pA+i]
      BY ConstAssumption DEF Phi2, Phi1, Phi0, I2, MA, TypeInv
      <2>0. QED BY ConstAssumption, <2>1, <2>2, <2>3,<2>4 DEF MA, Phi2, Phi1, Phi0, I2
   <1>5. MsA(ecx+1)'
     <2>a. SUFFICES  /\ (0..(ecx+1)-1) = (0..ecx-1) \cup {ecx}
                     /\ \A i \in (0..ecx-1) : mem'[pA+i] = mem0[pB+i]
                     /\ mem'[pA+ecx] = mem0[pB+ecx]
           BY ConstAssumption DEF MsA, Phi2, Phi1, Phi0, I2, MsA
     <2>1. (0..(ecx+1)-1) = (0..ecx-1) \cup {ecx}
        BY DEF Phi2, Phi1, Phi0
     <2>2. \A i \in (0..ecx-1) : mem[pA+i] = mem0[pB+i]
         BY DEF Phi2, Phi1, Phi0, I2, MsA
     <2>3. \A i \in (0..ecx-1) : mem[pA+i] = mem'[pA+i]
       BY ConstAssumption DEF Phi2, Phi1, Phi0, I2, TypeInv  
     <2>4. \A i \in (0..ecx-1) : mem'[pA+i] = mem0[pB+i]    
       BY ConstAssumption, <2>2, <2>3 DEF Phi2, Phi1, Phi0, I2, MsA  
     <2>6. mem'[pA+ecx] = mem0[pB+ecx]
      <3>0 QED BY ConstAssumption DEF Phi2, Phi1, Phi0, I2, MB, TypeInv
       
      <2>0. QED BY <2>a, <2>1, <2>4, <2>6, ConstAssumption DEF Phi2, I2, Phi1, Phi0, MsA , MA    
   <1>6. Phi2 /\ I2 => al' = mem0[pA+ecx]
      BY ConstAssumption DEF Phi2, Phi1, Phi0, I2, TypeInv, MA
   <1>0. QED BY <1>1, <1>2, <1>3, <1>4, <1>5, <1>6, ConstAssumption DEF Phi0, Phi1, Phi2, Phi3, I2, TypeInv 

LEMMA F3 == Phi3 /\ I3 => Phi4'
    <1> SUFFICES ASSUME Phi3 /\ I3 PROVE Phi4'
      BY DEF Phi3, I3, Phi4
    <1> USE DEF Phi3, I3, Phi4, TypeInv  
    <1> SUFFICES /\ MsA(ecx+1)'
                 /\ MA(ecx+1)'
                 /\ MsB(ecx+1)'
                 /\ MB(ecx+1)'
                 /\ TypeInv'
                 /\ ecx' \in (0..L-1)
     BY ConstAssumption
    <1>1 MsA(ecx+1)' 
     <2> SUFFICES \A i \in (0..ecx) : mem'[pA+i] = mem0[pB+i]
      BY ConstAssumption, I3, TypeInv, L1 DEF Phi2, Phi1, Phi0, MsA
      <2>1 \A i \in (0..ecx) : mem'[pA+i] = mem[pA+i]
          BY ConstAssumption, Phi3, I3, TypeInv, L1 DEF MsA
      <2>2 \A i \in (0..ecx) : mem[pA+i] = mem0[pB+i] BY DEF MsA    
     <2>0 QED BY ConstAssumption, <2>1, <2>2 DEF MsA      
    <1>2. MA(ecx+1)' 
     <2> SUFFICES \A i \in (ecx+1..L-1) : mem'[pA+i] = mem0[pA+i]
        BY ConstAssumption, I3, L1 DEF MA
     <2>1 \A  i \in (ecx+1..L-1) : mem'[pA+i] = mem[pA+i]
       BY ConstAssumption, I3
     <2>2 \A i \in (ecx+1..L-1) : mem[pA+i] = mem0[pA+i]
       BY ConstAssumption, I3 DEF Phi2, Phi1, Phi0, MA   
     <2>0 QED BY ConstAssumption, <2>1, <2>2 DEF MA   
    <1>3. MsB(ecx+1)'  
        <2>1 ecx \in (0..L-1)  BY Phi3, I3
        <2>2 MsB(ecx)' BY InvClosureA 
        <2>3 mem'[pB+ecx] = mem0[pA+ecx] BY ConstAssumption, Phi3, I3
        <2>4 ecx' = ecx BY I3 
        <2>0 QED BY ConstAssumption, <2>1,<2>2, <2>3, <2>4, ExtendSub DEF MsB, Phi3, I3
    <1>4 MB(ecx+1)'
        <2>1. MB(ecx)
      BY ConstAssumption DEF Phi2, Phi1, Phi0, MB
        <2>2. ecx \in (0..L-1) /\ ecx' = ecx
          BY ConstAssumption DEF Phi2, Phi1, Phi0, MB
        <2>3 mem \in [Nat ->BYTES]
          BY ConstAssumption DEF Phi3, TypeInv, MB          
        <2>4. mem' = [mem EXCEPT ![pB+ecx] = al]
          BY ConstAssumption DEF Phi3, I3
       <2>0. QED BY <2>1, <2>2, <2>3, <2>4, InvClosureB DEF Phi2, Phi1, Phi0, MB        
    <1>5 TypeInv'
     <2>0 QED BY Phi3, I3, TypeInv  
    <1>6 ecx' \in (0..L-1) BY Phi3, I3                    
    <1>0. QED BY <1>1, <1>2, <1>3, <1>4, ConstAssumption DEF MsA, MsB, MA, MB, TypeInv    

LEMMA F4 == Phi4 /\ I4 => Phi5'
   <1>0. QED BY DEF Phi4, I4 
    
LEMMA F5 == Phi5 /\ I5 => \/ (Phi7' /\ eip' = pm+27) 
                          \/ (Phi0' /\ eip' = pm)
    <1> SUFFICES ASSUME Phi5, I5 PROVE \/ (ecx = L /\ Phi7' /\ eip' = pm+27) 
                                       \/ (ecx \in (0..L-1) /\ Phi0' /\ eip' = pm)
                          OBVIOUS 
    <1> USE DEF Phi5, I5, TypeInv, Phi7                         
    <1>1. /\ ecx' \in (0..L)
           /\ mem' \in [Nat -> BYTES] 
           /\ al' \in BYTES
           /\ ah' \in BYTES 
           /\ ecx' = ecx
       BY Phi5, I5, TypeInv    
    <1>2 (ecx \in (0..L-1) \/ ecx = L)
       BY ConstAssumption, Phi5 
    <1> SUFFICES /\ ecx = L => Phi7' /\ eip' = pm+27
                 /\ ecx \in (0..L-1) => Phi0' /\ eip' = pm BY <1>2   
    <1>3 ASSUME ecx = L PROVE Phi7' /\ eip' = pm+27
     <2>1 MsA(L)' /\ MsB(L)' BY TypeInv, ecx=L, I5 DEF MsA, MsB
     <2>0 QED BY <2>1, ecx=L DEF I5, Phi5
    <1>4 ASSUME ecx \in (0..L-1) PROVE Phi0' /\ eip' = pm
        <2>1 MsA(ecx)' /\ MsB(ecx)' /\ MA(ecx)' /\ MB(ecx)' /\ TypeInv' /\ ecx' \in (0..L-1)
          BY I5, Phi5, ecx\in(0..L-1) DEF Phi0, MsA, MsB, MA, MB
        <2> QED BY <1>1, <2>1 DEF MsA, MsB, MA, MB, Phi0   
    <1>0. QED BY <1>1, <1>2, <1>3, <1>4 DEF Phi5, I5, Phi0                   
                          
                                   
                          
LEMMA InitPermute == Init => PermuteInvariant
   BY ConstAssumption DEF Init, PermuteInvariant, Phi0, MA, MB, MsA, MsB, TypeInv
   
LEMMA NextPermute == PermuteInvariant /\ [Next]_<<al, ah, ecx, mem, eip>> => PermuteInvariant'
   <1>1. SUFFICES /\ PermuteInvariant /\ I0 => PermuteInvariant'
                  /\ PermuteInvariant /\ I1 => PermuteInvariant'
                  /\ PermuteInvariant /\ I2 => PermuteInvariant'   
                  /\ PermuteInvariant /\ I3 => PermuteInvariant'
                  /\ PermuteInvariant /\ I4 => PermuteInvariant'   
                  /\ PermuteInvariant /\ I5 => PermuteInvariant'
                  /\ PermuteInvariant /\ UNCHANGED <<al, ah, ecx, mem, eip>> => PermuteInvariant'   
                  BY DEF Next
  <1>2 PermuteInvariant /\ I0 => PermuteInvariant'
  <2>1. I0 => eip = pm /\ eip' = pm+6
    BY DEF I0
  <2>2 PermuteInvariant /\ I0 => Phi1'
  BY <2>1, F0 DEF PermuteInvariant, I0
  <2>0. QED BY <2>1, <2>2, ConstAssumption DEF I0, Phi0, PermuteInvariant
  <1>3. PermuteInvariant /\ I1 => PermuteInvariant'
    <2>1 PermuteInvariant /\ I1 => eip = pm+6 /\ eip' = pm+12
    BY ConstAssumption DEF I1
    <2>2 PermuteInvariant /\ I1 => Phi2'
    BY <2>1, F1, ConstAssumption DEF PermuteInvariant
    <2>0. QED BY <2>1,<2>2 DEF PermuteInvariant, I1
  <1>4. PermuteInvariant /\ I2 => PermuteInvariant'
    <2>1 PermuteInvariant /\ I2 => eip = pm+12 /\ eip' = pm+18
    BY ConstAssumption DEF I2
    <2>2 PermuteInvariant /\ I2 => Phi3'
    BY <2>1, F2, ConstAssumption DEF PermuteInvariant, I2
    <2>0. QED BY <2>1,<2>2, F2, ConstAssumption DEF PermuteInvariant
  <1>5. PermuteInvariant /\ I3 => PermuteInvariant'
    <2>1 PermuteInvariant /\ I3 => eip = pm+18 /\ eip' =pm+19
    BY ConstAssumption DEF I3
    <2>2 PermuteInvariant /\ I3 => Phi4'
    BY <2>1, F3, ConstAssumption  DEF PermuteInvariant, I3
    <2>0. QED BY <2>1,<2>2 DEF PermuteInvariant
  <1>6. PermuteInvariant /\ I4 => PermuteInvariant'
    <2>1 PermuteInvariant /\ I4 => eip = pm+19 /\ eip' =pm+26
    BY ConstAssumption DEF I4
    <2>2 PermuteInvariant /\ I4 => Phi5'
    BY <2>1, F4 DEF PermuteInvariant, I4
    <2>0. QED BY <2>1,<2>2 DEF PermuteInvariant   
  <1>7. PermuteInvariant /\ I5 => PermuteInvariant'
   <2>1 PermuteInvariant /\ I5 => eip = pm+26 BY DEF PermuteInvariant, I5
   <2> SUFFICES ASSUME PermuteInvariant, I5 PROVE PermuteInvariant' OBVIOUS
   <2> USE DEF PermuteInvariant, I5
   <2> SUFFICES     /\ Phi7' 
                    /\ eip' = pm+27
                 \/ /\ Phi0'
                    /\ eip' = pm 
         BY PermuteInvariant
   <2>2 Phi5 BY PermuteInvariant, ConstAssumption, <2>1     
   <2>0 QED BY <2>2, F5 DEF PermuteInvariant
  <1>8. PermuteInvariant /\ UNCHANGED <<al, ah, ecx, mem, eip>> => PermuteInvariant' 
    BY DEF PermuteInvariant, Phi0, Phi1, Phi2, Phi3, Phi4, Phi5, Phi7, MA, MsA, MB, MsB, TypeInv                              
  <1>0. QED BY <1>1,<1>2, <1>3, <1>4,<1>5,<1>6,<1>7, <1>8 DEF Next, PermuteInvariant
                 

THEOREM PermutationCorrection == Init /\ [][Next]_<<al, ah, ecx,  mem, eip>> => []PermuteInvariant
BY InitPermute, NextPermute, PTL
                         

=============================================================================
\* Modification History
\* Last modified Wed Dec 30 10:04:02 CET 2015 by bonfante
\* Created Tue Dec 22 10:26:38 CET 2015 by bonfante
