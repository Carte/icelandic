---------------------------- MODULE permutation ----------------------------

EXTENDS Naturals, NaturalsInduction, TLAPS

BYTES == (0..255)

CONSTANTS K, L1, L2, pA, pB, pm, ptext, pstack, mem0, edi, ebp
(* K : 4 or 8
   L1, L2 : code length
   pA, pB : pointer on the initial code of A and B
   ptex : begining of text section
   pstack : position of the head of the stack, verified not being modified by the algorithm
   mem0 : initial state of the memory
   pm position of the code within the text section 
   edi, register containing pA+L1, verified not being modified, thus constant
   ebp : register pointing within the stack, verified not being modified by the algorithm
   *)

MEMORY_END == 256^K-1

ASSUME ConstAssumption == (* these are assumption about X86 and compilation requirements *)
       /\ K \in Nat \ {0}
       /\ pA \in (0..MEMORY_END) (* written p0 in the Icelandic paper *)
       /\ pB \in (0..MEMORY_END)
       /\ pB = pA+L1
       /\ L1 \in (1..MEMORY_END)
       /\ L2 \in (1..MEMORY_END)
       /\ pA+L1+L2 < MEMORY_END
       /\ mem0 \in [(0..MEMORY_END) -> BYTES]
       /\ pm \in (0..MEMORY_END)
       /\ ptext \in (0..MEMORY_END)
       /\ pm > ptext
       /\ ptext > pA + L1
       /\ ptext > pB + L2
       /\ pm + 25 < MEMORY_END
       /\ pstack \in (0..MEMORY_END) 
       /\ pstack > pm + 25 (* no buffer overflow at the begining *)
       /\ ebp \in (0..MEMORY_END)

ASSUME InitMemoryAssumption == (* assumption about the initial configuration *)
       /\ mem0[pm] = 57 (* the code of the loop *)
       /\ mem0[pm+1] = 249 
       /\ mem0[pm+2] = 124 
       /\ mem0[pm+3] = 8 
       /\ mem0[pm+4] = 43
       /\ mem0[pm+5] = 77
       /\ mem0[pm+6] = 12
       /\ mem0[pm+7] = 233
       /\ mem0[pm+8] = 3
       /\ mem0[pm+9] = 0
       /\ mem0[pm+10] = 0
       /\ mem0[pm+11] = 0
       /\ mem0[pm+12] = 3
       /\ mem0[pm+13] = 77
       /\ mem0[pm+14] = 16
       /\ mem0[pm+15] = 138
       /\ mem0[pm+16] = 33
       /\ mem0[pm+17] = 136
       /\ mem0[pm+18] = 1
       /\ mem0[pm+19] = 136
       /\ mem0[pm+20] = 224
       /\ mem0[pm+21] = 57
       /\ mem0[pm+22] = 241
       /\ mem0[pm+23] = 117
       /\ mem0[pm+24] = 231    
       /\ mem0[pm+25] = 2  
       /\ edi = pA+L1
       /\ ebp > pstack (* ebp refers to the stack *)
       /\ ebp + 20 <= MEMORY_END
       /\ mem0[ebp + 12] = L1 % 256 (* little endian *)
       /\ mem0[ebp + 13] = (L1 \div 256) % 256 
       /\ mem0[ebp + 14] = (L1 \div 256^2) % 256
       /\ mem0[ebp + 15] = (L1 \div 256^3) % 256
       /\ mem0[ebp + 16] = L2 % 256 (* little endian *)
       /\ mem0[ebp + 17] = (L2 \div 256) % 256 
       /\ mem0[ebp + 18] = (L2 \div 256^2) % 256
       /\ mem0[ebp + 19] = (L2 \div 256^3) % 256
       
VARIABLES eip, al, ah, ecx, mem       
       
sigma[i \in (0..L1+L2-1)] == IF i >= L1 THEN i - L1 ELSE i+L2
sigma_it[n \in Nat] == IF n = 0 THEN 0 ELSE sigma[sigma_it[n-1]]
       
Init == 
    /\ ecx=pA
    /\ eip = pm
    /\ mem = mem0
    /\ ah \in BYTES
    /\ al = mem[ecx]

(*
    invert_pgcd_loopswitch:
      ; Test pointer position, in code of A or B
      cmp ecx, edi
      jl invert_pgcd_sidansa 
      
      ; If ecx < edi
      sub ecx, [ebp + 12] ; ecx = ecx - size1
      jmp invert_pgcd_endsidansa

      ; If ecx >= edi
      invert_pgcd_sidansa:
      add ecx, [ebp + 16] ; ecx = ecx + size2

      ; Inversion
      invert_pgcd_endsidansa:

      ; keep the current pointer content in (ah)
      mov ah, [ecx] ; ah = *ecx

      ; Writing
      mov [ecx], al ; *ecx = al

      ; New value to be moved
      mov al, ah
      
      ; Test de fin de boucle
      cmp ecx, esi
      jne invert_pgcd_loopswitch

    ; Incrementation
    add edx, 1

    invert_covering_test:
    cmp edx, [ebp - 4]
    jl invert_covering_loop
 *)

I0 == 
   \/ /\ mem[eip+0] = 57 
      /\ mem[eip+1] = 249 
      /\ mem[eip+2] = 124 
      /\ mem[eip+3] = 8  (* 39 F9 7C 08 *)
      /\ ecx < edi
      /\ eip' = eip+12
      /\ UNCHANGED <<al, ah, ecx, mem>>
   \/ /\ mem[eip] = 57 
      /\ mem[eip+1] = 249 
      /\ mem[eip+2] = 124 
      /\ mem[eip+3] = 8  (* 39 F9 7C 08 *)
      /\ ecx >= edi
      /\ eip' = eip+4
      /\ UNCHANGED <<al, ah, ecx, mem>> 

I1 == /\ mem[eip+0] = 43
      /\ mem[eip+1] = 77
      /\ mem[eip+2] = 12 (* 2B 4D 0C *)
      /\ ecx' = ecx - (mem[ebp+12] + 256 * mem[ebp+13] + 256^2 * mem[ebp+14] + 256^3 * mem[ebp+15])
      /\ eip' = eip+3
      /\ UNCHANGED <<al, ah, mem>>
      
I2 == /\ mem[eip+0] = 233
      /\ mem[eip+1] = 3
      /\ mem[eip+2] = 0
      /\ mem[eip+3] = 0
      /\ mem[eip+4] = 0 (* E9 03 00 00 00 *)
      /\ eip' = eip + 8
      /\ UNCHANGED <<al, ah, ecx, mem>>      
      
I3 == /\ mem[eip+0] = 3
      /\ mem[eip+1] = 77
      /\ mem[eip+2] = 16 (* 03 4D 10 *)
      /\ eip' = eip + 3
      /\ ecx' = ecx + (mem[ebp+16] + 256 * mem[ebp+17] + 256^2 * mem[ebp+18] + 256^3 * mem[ebp+19])
      /\ UNCHANGED <<al, ah, mem>>
      
I4 == /\ mem[eip+0] = 138
      /\ mem[eip+1] = 33   (* 8A 21 *)
      /\ eip' = eip + 2
      /\ ah' = mem[ecx]
      /\ UNCHANGED <<al, ecx, mem>>

I5 == /\ mem[eip+0] = 136
      /\ mem[eip+1] = 1(* 88 01 *)
      /\ eip' = eip+2
      /\ mem' = [mem EXCEPT ![ecx] = al]
      /\ UNCHANGED <<al, ah, ecx>>

I6 == /\ mem[eip+0] = 136
      /\ mem[eip+1] = 224 (* 88 E0 *)
      /\ eip' = eip + 2
      /\ al' = ah
      /\ UNCHANGED <<ah, ecx, mem>>  

I7 == \/ /\ mem[eip+0] = 57
         /\ mem[eip+1] = 241
         /\ mem[eip+2] = 117
         /\ mem[eip+3] = 231 (* 39 F1 75 E7 *)
         /\ ecx = pA
         /\ eip' = eip + 4
         /\ UNCHANGED <<al, ah, ecx, mem>>
      \/ /\ mem[eip+0] = 57
         /\ mem[eip+1] = 241
         /\ mem[eip+2] = 117
         /\ mem[eip+3] = 231
         /\ \neg ecx = pA
         /\ eip' = eip - 21
         /\ UNCHANGED <<al, ah, ecx, mem>>
      
Next == I1 \/ I2 \/ I3 \/ I4 \/ I5 \/ I6 \/ I7
                  
TypeInv == /\ ecx \in (0..MEMORY_END)
           /\ mem \in [(0..MEMORY_END) -> BYTES]
           /\ al \in BYTES
           /\ ah \in BYTES
           /\ eip \in (0..MEMORY_END)

DomInv == 
           /\ ecx \in (pA..pA+L1+L2-1)
           /\ \A i \in (0..MEMORY_END) \ (pA..pA+L1+L2-1) : mem[i] = mem0[i]
           /\ eip \in (pm..pm+25)
          
Psi0(d) == /\ ecx = pA+sigma_it[d] 
           /\ \A j \in (0..d-1)     : mem[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]]
           /\ \A j \in (d+1..L1+L2-1) : mem[pA+sigma_it[j]] = mem0[pA+sigma_it[j]]
           /\ al = mem0[pA+sigma_it[d]]      
                  
Phi0 == /\ TypeInv /\ DomInv
        /\ \E i \in (0..L1+L2-1) : Psi0(i)
        /\ eip = pm

Phi1 == /\ TypeInv /\ DomInv
        /\ \E i \in (0..L1+L2-1) : Psi0(i)
        /\ \/ /\ eip = pm+12 
              /\ ecx < pA+L1
           \/ /\ eip = pm+4
              /\ ecx >= pA+L1   

Psi2(d) == /\ ecx = pA + sigma_it[d+1] 
           /\ \A j \in (0..d-1)     : mem[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]]
           /\ \A j \in (d+1..L1+L2-1) : mem[pA+sigma_it[j]] = mem0[pA+sigma_it[j]]
           /\ al = mem0[pA+sigma_it[d]]
           
Phi2 == /\ TypeInv /\ DomInv
        /\ \E i \in (0..L1+L2-1) : Psi2(i)
        /\ \/ eip = pm+7
           \/ eip = pm+15
        
Psi4(d) == /\ ecx = pA + sigma_it[d+1] 
           /\ \A j \in (0..d-1)     : mem[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]]
           /\ \A j \in (d+1..L1+L2-1) : mem[pA+sigma_it[j]] = mem0[pA+sigma_it[j]]
           /\ al = mem0[pA+sigma_it[d]]
           /\ d+1 <= L1+L2-1 => ah = mem0[pA+sigma_it[d+1]]  

Phi3 == /\ TypeInv /\ DomInv
        /\ \E i \in (0..L1+L2-1) : Psi4(i)
        /\ eip = pm+17     
 
Psi5(d) == /\ ecx = pA + sigma_it[d] 
           /\ \A j \in (0..d-1)     : mem[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]]
           /\ \A j \in (d+1..L1+L2-1) : mem[pA+sigma_it[j]] = mem0[pA+sigma_it[j]]
           /\ d <= L1+L2-1 => ah = mem0[pA+sigma_it[d]] 

Phi4 == /\ TypeInv /\ DomInv
        /\ \E i \in (0..L1+L2) : Psi5(i)
        /\ eip = pm + 19   
        
Psi6(d) == /\ ecx = pA + sigma_it[d] 
           /\ \A j \in (0..d-1)     : mem[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]]
           /\ \A j \in (d+1..L1+L2-1) : mem[pA+sigma_it[j]] = mem0[pA+sigma_it[j]]
           /\ d <= L1+L2-1 => al = mem0[pA+sigma_it[d]] 

Phi5 == /\ TypeInv /\ DomInv
        /\ \E i \in (0..L1+L2) : Psi6(i) 
        /\ eip = pm + 21                   
                   
Phi6 == /\ TypeInv /\ DomInv
        /\ \A j \in (0..L1+L2-1) : mem[pA+sigma[j]] = mem0[pA+j]
        /\ eip = pm + 25                              

THEOREM Sigma_itDefConclusion == NatInductiveDefConclusion(sigma_it, 0, LAMBDA v,n : sigma[v])
  <1>1. NatInductiveDefHypothesis(sigma_it, 0, LAMBDA v, n : sigma[v]) 
    BY DEF NatInductiveDefHypothesis, sigma_it, sigma
  <1>0 QED BY <1>1, NatInductiveDef  
         
THEOREM Sigma_itDef == \A n \in Nat : sigma_it[n] = IF n = 0 THEN 0 ELSE sigma[sigma_it[n-1]]
BY Sigma_itDefConclusion DEF NatInductiveDefConclusion

THEOREM Sigma_itType == sigma_it \in [Nat -> (0..L1+L2-1)]
 <1>1 \A v \in (0..L1+L2-1), n \in Nat \ {0} : sigma[v] \in (0..L1+L2-1) BY ConstAssumption DEF sigma
 <1>2 0 \in (0..L1+L2-1) BY ConstAssumption
 <1>0 QED BY <1>1, <1>2, NatInductiveDefType, Sigma_itDefConclusion, Isa

LEMMA initSigma == sigma_it[0] = 0  
BY Sigma_itDef

LEMMA stableSigma == ASSUME NEW x \in (0..L1+L2) PROVE sigma_it[x] \in (0..L1+L2-1)
BY ConstAssumption, Sigma_itType

LEMMA stableSigmab == ASSUME NEW x \in (0..L1+L2) PROVE sigma_it[x] \in (0..MEMORY_END)
<1> sigma_it[x] \in (0..L1+L2-1) BY Sigma_itType
<1> \A i \in (0..L1+L2-1) : i \in (0..MEMORY_END) BY ConstAssumption DEF MEMORY_END
<1>0 QED OBVIOUS

LEMMA nextSigma == ASSUME NEW x \in (0..L1+L2-1) PROVE sigma[sigma_it[x]] = sigma_it[x+1]
BY Sigma_itDef

(* these are lemmas about mathematics, no need to reprove them :) *)
LEMMA nextHighSigma == ASSUME NEW x \in (0..L1+L2-1), sigma_it[x] \in (L1..L1+L2-1)
                       PROVE sigma_it[x+1] = sigma_it[x] - L1
OMITTED
                       
LEMMA nextSmallSigma == ASSUME NEW x \in (0..L1+L2-1), sigma_it[x] \in (0..L1-1)
                       PROVE sigma_it[x+1] = sigma_it[x] + L2
                       OMITTED                       

LEMMA SigmaIsInjective == ASSUME NEW x \in (0..L1+L2), NEW y \in (0..L1+L2)
                          PROVE x # y => sigma_it[x] # sigma_it[y]
OMITTED  

LEMMA sigma_itLoop == ASSUME NEW x \in (0..L1+L2-1) 
                    PROVE sigma_it[x] = 0 => x = L1+L2
                    OMITTED

LEMMA sigma_itn == sigma_it[L1+L2] = 0 OMITTED                    
                              

LEMMA SigmaSwap == ASSUME \A j \in (0..L1+L2-1) : mem[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]]
                   PROVE \A j \in (0..L1+L2-1) : mem[pA+sigma[j]] = mem0[pA+j]
                   OMITTED
                 
LEMMA IntDecomposition == ASSUME NEW j \in (0..MEMORY_END) 
          PROVE j = (j % 256) + 256 * ((j \div 256) % 256) + 256^2 * ((j \div 256^2) % 256) + 256^3 * ((j \div 256^3) % 256)
          OMITTED 
                 
LEMMA L8 == ASSUME TypeInv, DomInv, 
                   mem' = [mem EXCEPT ![ecx] = al], 
                   NEW i \in (0..L1+L2-1), 
                   pA+sigma_it[i] # ecx
         PROVE  mem'[pA+sigma_it[i]] = mem[pA+sigma_it[i]]
   
   <1> USE DEF TypeInv, DomInv      
   <1> DEFINE sn == sigma_it[i]      
   <1> sn \in (0..L1+L2-1) BY ConstAssumption, stableSigma DEF TypeInv
   <1> HIDE DEF sn
   <1> pA + sn # ecx BY DEF sn
   <1>1 pA + sn < pA + L1+L2 BY sn \in (0..L1+L2-1), ConstAssumption
   <1>2 sn \in Nat BY DEF sn
   <1>3 pA + sn \in Nat BY ConstAssumption
   <1>4 pA + L1 + L2 \in Nat BY ConstAssumption
   <1>5 MEMORY_END \in Nat BY ConstAssumption DEF MEMORY_END
   <1>6 pA + sn <= MEMORY_END 
   BY ConstAssumption, <1>1, pA+L1+L2< MEMORY_END, <1>2, <1>3, <1>4, <1>5
   <1>7 pA + sn >= 0 BY ConstAssumption
   <1> pA + sn \in (0..MEMORY_END) 
   BY <1>6, <1>7, ConstAssumption
   <1> mem'[pA+sn] = mem[pA+sn]
     BY mem' = [mem EXCEPT ![ecx] = al], pA + sn # ecx, ConstAssumption
   <1> QED BY DEF sn


LEMMA L9 == ASSUME TypeInv,
                   mem' = [mem EXCEPT ![ecx] = al] 
            PROVE mem'[ecx] = al
BY ConstAssumption DEF TypeInv

LEMMA L10 == ASSUME NEW x \in (0..L1+L2) PROVE x \in (0..L1+L2-1) \/ x = L1+L2
BY ConstAssumption

LEMMA L11 ==ASSUME NEW x \in (0..MEMORY_END), pA+x \in (pA+L1..pA+L1+L2-1) PROVE  x \in (L1..L1+L2-1)
BY ConstAssumption
 
LEMMA L12 == ASSUME NEW x \in (0..MEMORY_END), pA+x < pA+L1 PROVE x \in (0..L1-1)
BY ConstAssumption

LEMMA L13 == ASSUME NEW x \in Nat, NEW y \in Nat PROVE x # y => pA+x # pA+y
BY ConstAssumption
 
LEMMA StableCode == ASSUME TypeInv, DomInv 
   PROVE /\ mem[eip] = mem0[eip]
         /\ mem[eip+1] = mem0[eip+1]
   <1> USE DEF TypeInv, DomInv      
   <1>1 eip > pA+L1+L2-1 /\ eip+1 > pA+L1+L2-1 BY ConstAssumption 
   <1>2 eip \in (0..MEMORY_END) \ (pA..pA+L1+L2-1) BY <1>1, ConstAssumption DEF MEMORY_END
   <1>3 eip +1 \in (0..MEMORY_END) \ (pA..pA+L1+L2-1) BY <1>1, ConstAssumption DEF MEMORY_END
   <1>0 QED BY <1>2, <1>3, ConstAssumption, TypeInv DEF MEMORY_END

LEMMA L110 == ASSUME TypeInv, DomInv, Next, eip = pm PROVE I0
  <2>1 mem[eip] = 57 /\ mem[eip+1] = 249 BY StableCode, ConstAssumption, InitMemoryAssumption
  <2>  \neg (I1 \/ I2 \/ I3 \/ I4 \/ I5 \/ I6 \/ I7)  BY <2>1 DEF I1, I2, I3, I4, I5, I6, I7
  <2>0 QED BY DEF Next, TypeInv

LEMMA L114 == ASSUME TypeInv, DomInv, Next, eip = pm + 4 PROVE I1
  <2>1 mem[eip] = 43 /\ mem[eip+1] = 77 BY StableCode, ConstAssumption, InitMemoryAssumption
  <2>  \neg (I0 \/ I2 \/ I3 \/ I4 \/ I5 \/ I6 \/ I7)  BY <2>1 DEF I0, I1, I2, I3, I4, I5, I6, I7
  <2>0 QED BY DEF Next, TypeInv

LEMMA L113 == ASSUME TypeInv, DomInv, Next, eip = pm + 12 PROVE I3
  <2>1 mem[eip] = 3 /\ mem[eip+1] = 77 BY StableCode, ConstAssumption, InitMemoryAssumption
  <2>  \neg (I0 \/ I1 \/ I2 \/ I4 \/ I5 \/ I6 \/ I7)  BY <2>1 DEF I0, I1, I2, I3, I4, I5, I6, I7
  <2>0 QED BY DEF Next, TypeInv
  
LEMMA L117 == ASSUME TypeInv, DomInv, Next, eip = pm + 7 PROVE I2
  <2>1 mem[eip] = 233 /\ mem[eip+1] = 3 BY StableCode, ConstAssumption, InitMemoryAssumption
  <2>  \neg (I0 \/ I1 \/ I3 \/ I4 \/ I5 \/ I6 \/ I7)  BY <2>1 DEF I0, I1, I2, I3, I4, I5, I6, I7
  <2>0 QED BY DEF Next, TypeInv  
              
LEMMA L125 == ASSUME TypeInv, DomInv, Next, eip = pm + 15 PROVE I4
  <2>1 mem[eip] = 138 /\ mem[eip+1] = 33 BY StableCode, ConstAssumption, InitMemoryAssumption
  <2>  \neg (I0 \/ I1 \/ I2 \/ I3 \/ I5 \/ I6 \/ I7)  BY <2>1 DEF I0, I1, I2, I3, I4, I5, I6, I7
  <2>0 QED BY DEF Next, TypeInv 
  
LEMMA L127 == ASSUME TypeInv, DomInv, Next, eip = pm + 17 PROVE I5
  <2>1 mem[eip] = 136 /\ mem[eip+1] = 1 BY StableCode, ConstAssumption, InitMemoryAssumption
  <2>  \neg (I0 \/ I1 \/ I2 \/ I3 \/ I4 \/ I6 \/ I7)  BY <2>1 DEF I0, I1, I2, I3, I4, I5, I6, I7
  <2>0 QED BY DEF Next, TypeInv 
 
LEMMA L129 == ASSUME TypeInv, DomInv, Next, eip = pm + 19 PROVE I6
  <2>1 mem[eip] = 136 /\ mem[eip+1] = 224 BY StableCode, ConstAssumption, InitMemoryAssumption
  <2>  \neg (I0 \/ I1 \/ I2 \/ I3 \/ I4 \/ I5 \/ I7)  BY <2>1 DEF I0, I1, I2, I3, I4, I5, I6, I7
  <2>0 QED BY DEF Next, TypeInv
  
LEMMA L131 == ASSUME TypeInv, DomInv, Next, eip = pm + 21 PROVE I7
  <2>1 mem[eip] = 57 /\ mem[eip+1] = 241 BY StableCode, ConstAssumption, InitMemoryAssumption
  <2>  \neg (I0 \/ I1 \/ I2 \/ I3 \/ I4 \/ I5 \/ I6)  BY <2>1 DEF I0, I1, I2, I3, I4, I5, I6, I7
  <2>0 QED BY DEF Next, TypeInv   
    
LEMMA F0 == Phi0 /\ Next => Phi1'
  <1> SUFFICES ASSUME Phi0, Next PROVE Phi1' OBVIOUS
  <1> USE DEF Phi0, Next, I0, Phi1, TypeInv, DomInv
  <1> I0 BY Next, TypeInv, L110
  <1> SUFFICES /\ TypeInv' /\ DomInv'
             /\ \E i \in (0..L1+L2-1) : Psi0(i)'
             /\  \/ /\ eip' = pm+12 
                    /\ ecx' < pA+L1
                 \/ /\ eip' = pm+4
                    /\ ecx' >= pA+L1  BY DEF Phi1
  <1> eip' = pm + 4 \/ eip' = pm + 12 OBVIOUS
  <1>11 eip' \in Nat /\ eip' < pm + 25 BY ConstAssumption
  <1> pm + 25 \in Nat BY ConstAssumption
  <1> eip' \in (0..MEMORY_END) 
      BY ConstAssumption, <1>11, pm + 25 \in Nat, pm + 25 < MEMORY_END DEF MEMORY_END         
  <1> TypeInv' BY DEF I0, Phi0, TypeInv
  <1> \E i \in (0..L1+L2-1) : Psi0(i)' BY DEF I0, Psi0   (* wtf *)              
  <1>1 ecx < pA+L1 \/ ecx >= pA+L1 BY ConstAssumption, TypeInv                  
  <1>2 ecx < pA+L1 => /\ eip' = eip+12
                      /\ ecx' < pA+L1 BY I0, ConstAssumption, InitMemoryAssumption
  <1>3 ecx >= pA+L1 => /\ eip' = eip+4
                       /\ ecx >= pA+L1 BY I0, ConstAssumption, InitMemoryAssumption  
  <1>0 QED BY <1>1, <1>2, <1>3                     
      
LEMMA EbpIsBiggerThanData == ASSUME DomInv, NEW i \in Nat PROVE ebp +12 + i > pA+L1+L2-1
  <1> USE DEF DomInv 
  <1> ebp \in Nat BY ConstAssumption
  <1>1 ebp + 12 > pstack BY ConstAssumption, InitMemoryAssumption
  <1>2 pstack > pm BY ConstAssumption
  <1>3 pm > pA+L1+L2-1 BY ConstAssumption 
  <1> QED BY ConstAssumption, <1>1, <1>2, <1>3
        
LEMMA Ebp12ContainsL1 == ASSUME I1, TypeInv, DomInv PROVE ecx' = ecx - L1
 <1> USE DEF I1, TypeInv, DomInv
 <1>1 ecx' = ecx - (mem[ebp+12] + 256 * mem[ebp+13] + 256^2 * mem[ebp+14] + 256^3 * mem[ebp+15]) BY I1
 <1> ebp\in Nat BY ConstAssumption
 <1> ASSUME NEW i \in (0..3) PROVE mem[ebp+12+i] = mem0[ebp+12+i]
    <2> i \in Nat OBVIOUS
    <2>2 ebp + 12 + i > pA+L1+L2-1 BY EbpIsBiggerThanData
    <2>3 \neg (ebp + 12 + i \in (pA..pA+L1+L2-1)) BY <2>2, ConstAssumption 
    <2>4 ebp + 12 + i \in Nat BY ConstAssumption
    <2>5 ebp + 12 +i <= ebp+20 BY ConstAssumption
    <2>6 MEMORY_END \in Nat BY ConstAssumption DEF MEMORY_END
    <2>7 ebp + 12 + i <= MEMORY_END BY <2>5, <2>6, ConstAssumption, InitMemoryAssumption
    <2>8 ebp + 12 +i \in (0..MEMORY_END)  \ (pA..pA+L1+L2-1) BY <2>3, <2>4, <2>7
    <2> QED BY <2>8, DomInv, TypeInv
 <1>2 /\ mem[ebp+12] = mem0[ebp+12]  
      /\ mem[ebp+12+1] = mem0[ebp+12+1]
      /\ mem[ebp+12+2] = mem0[ebp+12+2]
      /\ mem[ebp+12+3] = mem0[ebp+12+3] OBVIOUS
 <1>3 /\ mem[ebp+12] = mem0[ebp+12]  
      /\ mem[ebp+13] = mem0[ebp+13]
      /\ mem[ebp+14] = mem0[ebp+14]
      /\ mem[ebp+15] = mem0[ebp+15] BY <1>2
 <1>4 ecx' = ecx - (mem0[ebp+12] + 256 * mem0[ebp+13] + 256^2 * mem0[ebp+14] + 256^3 * mem0[ebp+15]) BY <1>1, <1>3
 <1>5 ecx' = ecx - ((L1 % 256) + 256 * ((L1 \div 256) % 256) + 256^2 * ((L1 \div 256^2) % 256) + 256^3 * ((L1\div 256^3) % 256))
   BY <1>4, InitMemoryAssumption
 <1> L1 = ((L1 % 256) + 256 * ((L1 \div 256) % 256) + 256^2 * ((L1 \div 256^2) % 256) + 256^3 * ((L1\div 256^3) % 256)) 
     BY ConstAssumption, IntDecomposition 
 <1> QED BY <1>5    

LEMMA Ebp16ContainsL2 == ASSUME I3, TypeInv, DomInv PROVE ecx' = ecx + L2
 <1> USE DEF I3, TypeInv, DomInv
 <1>1 ecx' = ecx + (mem[ebp+16] + 256 * mem[ebp+17] + 256^2 * mem[ebp+18] + 256^3 * mem[ebp+19]) BY I3
 <1> ebp\in Nat BY ConstAssumption
 <1> ASSUME NEW i \in (4..7) PROVE mem[ebp+12+i] = mem0[ebp+12+i]
    <2> i \in Nat OBVIOUS
    <2>2 ebp + 12 + i > pA+L1+L2-1 BY EbpIsBiggerThanData
    <2>3 \neg (ebp + 12 + i \in (pA..pA+L1+L2-1)) BY <2>2, ConstAssumption 
    <2>4 ebp + 12 + i \in Nat BY ConstAssumption
    <2>5 ebp + 12 +i <= ebp+20 BY ConstAssumption
    <2>6 MEMORY_END \in Nat BY ConstAssumption DEF MEMORY_END
    <2>7 ebp + 12 + i <= MEMORY_END BY <2>5, <2>6, ConstAssumption, InitMemoryAssumption
    <2>8 ebp + 12 +i \in (0..MEMORY_END)  \ (pA..pA+L1+L2-1) BY <2>3, <2>4, <2>7
    <2> QED BY <2>8, DomInv, TypeInv
 <1>2 /\ mem[ebp+12+4] = mem0[ebp+12+4]  
      /\ mem[ebp+12+5] = mem0[ebp+12+5]
      /\ mem[ebp+12+6] = mem0[ebp+12+6]
      /\ mem[ebp+12+7] = mem0[ebp+12+7] OBVIOUS
 <1>3 /\ mem[ebp+16] = mem0[ebp+16]  
      /\ mem[ebp+17] = mem0[ebp+17]
      /\ mem[ebp+18] = mem0[ebp+18]
      /\ mem[ebp+19] = mem0[ebp+19] BY <1>2
 <1>4 ecx' = ecx + (mem0[ebp+16] + 256 * mem0[ebp+17] + 256^2 * mem0[ebp+18] + 256^3 * mem0[ebp+19]) BY <1>1, <1>3
 <1>5 ecx' = ecx + ((L2 % 256) + 256 * ((L2 \div 256) % 256) + 256^2 * ((L2 \div 256^2) % 256) + 256^3 * ((L2\div 256^3) % 256))
   BY <1>4, InitMemoryAssumption
 <1> L2 = ((L2 % 256) + 256 * ((L2 \div 256) % 256) + 256^2 * ((L2 \div 256^2) % 256) + 256^3 * ((L2\div 256^3) % 256)) 
     BY ConstAssumption, IntDecomposition 
 <1> QED BY <1>5
        
LEMMA F1 == Phi1 /\ Next => Phi2'
    <1> SUFFICES ASSUME Phi1, Next PROVE Phi2' OBVIOUS
    <1> USE DEF Phi1, Phi2, TypeInv, DomInv, Psi2, Psi0
    <1> SUFFICES TypeInv' /\ DomInv' /\ \E i \in (0..L1+L2-1) : Psi2(i)' /\ (eip' = pm+7 \/ eip' = pm+15) BY DEF Phi2
    <1> eip = pm+4 \/ eip = pm+12 BY Phi1
    <1>1 ASSUME eip = pm+4 PROVE Phi2'
      <2> I1 BY Next, TypeInv, L114, eip = pm + 4
      <2> eip #pm+12 BY ConstAssumption, eip = pm + 4
      <2> ecx >= pA+L1 BY Phi1
      <2>2 ecx' = ecx - L1 BY Ebp12ContainsL1
      <2>3 ecx' \in (pA..pA+L1+L2-1) BY ConstAssumption, TypeInv,<2>2
      <2>4 TypeInv'  BY ConstAssumption, <2>2, <2>3 DEF I1, TypeInv, MEMORY_END
      <2>5 eip'= pm+7 BY ConstAssumption DEF I1
      <2>6 \E i \in (0..L1+L2-1) : Psi2(i)'
          <3> SUFFICES \E i \in (0..L1+L2-1) : 
           /\ ecx' = pA + sigma_it[i+1] 
           /\ \A j \in (0..i-1)     : mem'[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]]
           /\ \A j \in (i+1..L1+L2-1) : mem'[pA+sigma_it[j]] = mem0[pA+sigma_it[j]]
           /\ al' = mem0[pA+sigma_it[i]]
             BY DEF Psi2
          <3> PICK ii \in (0..L1+L2-1) : Psi0(ii) BY Phi1
          <3> WITNESS ii \in (0..L1+L2-1)
          <3> /\ \A j \in 0..ii - 1 : (mem')[pA + sigma_it[j + 1]] = mem0[pA + sigma_it[j]]
              /\ \A j \in ii + 1..L1 + L2 - 1 :(mem')[pA + sigma_it[j]] = mem0[pA + sigma_it[j]]
              /\ al' = mem0[pA + sigma_it[ii]] BY DEF I1, Psi0
          <3>1 pA+sigma_it[ii] \in (pA+L1..pA+L1+L2-1) BY ecx >= pA+L1, TypeInv
          <3>2 sigma_it[ii] \in (0..MEMORY_END) BY stableSigmab, ii \in (0..L1+L2-1), ConstAssumption
          <3> sigma_it[ii] \in (L1..L1+L2-1) BY <3>1, <3>2, L11
          <3>3 sigma_it[ii+1] = sigma_it[ii] - L1 BY nextHighSigma
          <3>5 ecx = pA+ sigma_it[ii] BY DEF Psi0
          <3>. DEFINE sn == sigma_it[ii]
          <3>. sn \in (0..MEMORY_END)  BY <3>2
          <3>. ecx = pA + sn  BY <3>5
          <3>. HIDE DEF sn
          <3>. ecx' = pA + sn - L1 BY <2>2, ConstAssumption
          <3>6 ecx' = pA+sigma_it[ii] - L1 BY DEF sn \*ONLY ConstAssumption, ecx \in (0..MEMORY_END), <3>2, <3>4, <3>5
          <3> ecx' = pA+sigma_it[ii+1] BY ConstAssumption, <3>6, nextHighSigma
           <3>0 QED BY DEF Psi2
      <2> DomInv' BY ConstAssumption, <2>2, <2>3 DEF I1, TypeInv, DomInv, MEMORY_END
      <2>0 QED BY <2>4, <2>5, <2>6
    <1>2 ASSUME eip = pm + 12 PROVE Phi2'
      <2> TypeInv /\ Next OBVIOUS
      (* eip = pm + 12 *)
      <2> I3 BY Next, TypeInv, eip = pm+12, L113
      <2> eip #pm+4 BY ConstAssumption, eip = pm + 12
      <2> ecx < pA+L1 BY Phi1
      <2>2 ecx' = ecx + L2 BY Ebp16ContainsL2
      <2>3 ecx' \in (pA..pA+L1+L2-1) BY ConstAssumption, TypeInv,<2>2
      <2>4 TypeInv' BY ConstAssumption, <2>3 DEF I3, TypeInv, DomInv, MEMORY_END
      <2>5 eip'= pm+15 BY ConstAssumption DEF I3
      <2>6 \E i \in (0..L1+L2-1) : Psi2(i)'
          <3> SUFFICES \E i \in (0..L1+L2-1) : 
           /\ ecx' = pA + sigma_it[i+1] 
           /\ \A j \in (0..i-1)     : mem'[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]]
           /\ \A j \in (i+1..L1+L2-1) : mem'[pA+sigma_it[j]] = mem0[pA+sigma_it[j]]
           /\ al' = mem0[pA+sigma_it[i]]
             BY DEF Psi2
          <3> PICK ii \in (0..L1+L2-1) : Psi0(ii) BY Phi1
          <3> WITNESS ii \in (0..L1+L2-1)
          <3> /\ \A j \in 0..ii - 1 : (mem')[pA + sigma_it[j + 1]] = mem0[pA + sigma_it[j]]
              /\ \A j \in ii + 1..L1 + L2 - 1 :(mem')[pA + sigma_it[j]] = mem0[pA + sigma_it[j]]
              /\ al' = mem0[pA + sigma_it[ii]] BY DEF I3, Psi0
          <3>1 pA+sigma_it[ii] < pA+L1 BY ecx < pA+L1, TypeInv
          <3>2 sigma_it[ii] \in (0..MEMORY_END) BY stableSigmab, ii \in (0..L1+L2-1), ConstAssumption
          <3> sigma_it[ii] \in (0..L1-1) BY <3>1, <3>2, L12
          <3>3 sigma_it[ii+1] = sigma_it[ii] + L2 BY nextSmallSigma
          <3>5 ecx = pA+ sigma_it[ii] BY DEF Psi0
          <3>. DEFINE sn == sigma_it[ii]
          <3>. sn \in (0..MEMORY_END)  BY <3>2
          <3>. ecx = pA + sn  BY <3>5
          <3>. HIDE DEF sn
          <3>. DEFINE snp == sigma_it[ii+1]
          <3>51 snp = sn + L2 BY <3>3 DEF sn
          <3>52 ecx' = pA + sn + L2 BY <2>2, ConstAssumption
          <3>6 ecx' = pA+snp BY <3>51, <3>52, ConstAssumption \*ONLY ConstAssumption, ecx \in (0..MEMORY_END), <3>2, <3>4, <3>5
          <3> ecx' = pA+sigma_it[ii+1] BY ConstAssumption, <3>3, <3>6
           <3>0 QED BY DEF Psi2
      <2> DomInv' BY ConstAssumption, <2>3 DEF I3, DomInv, MEMORY_END     
      <2>0 QED BY <2>4, <2>5, <2>6
    <1>0 QED BY <1>1, <1>2 DEF Phi2

      
LEMMA F2 == Phi2 /\ Next => Phi2' \/ Phi3'
  <1> SUFFICES ASSUME Phi2, Next PROVE Phi2' \/ Phi3' OBVIOUS
  <1> USE DEF Phi2, Psi2, Next
  <1>1 eip = pm + 7 \/ eip = pm + 15 BY Phi2
  <1>2 CASE eip = pm + 7
   <2>. I2 BY TypeInv, Next, eip = pm + 7, L117
   <2> USE DEF I2
   <2>. eip' = pm+15 BY ConstAssumption, eip = pm + 7 DEF I2
   <2>11 eip' \in Nat /\ eip' < pm + 25  BY ConstAssumption, pm \in Nat DEF I2
   <2>. eip' \in (0..MEMORY_END) 
     BY ConstAssumption, <2>11, pm + 25 < MEMORY_END, pm + 25 \in Nat DEF MEMORY_END
   <2>. TypeInv' BY ConstAssumption, Phi2 DEF TypeInv, I2
   <2> DomInv' BY ConstAssumption, Phi2 DEF DomInv, I2
   <2>. \E i \in (0..L1+L2-1) : Psi2(i)'
      BY ConstAssumption, Phi2 DEF I2, Psi2
   <2>0 QED OBVIOUS
  <1>3 CASE eip = pm + 15
    <2>1 I4 BY TypeInv, Next, eip = pm + 15, L125
    <2> SUFFICES Phi3' OBVIOUS
    <2> USE DEF Phi3, I4
    <2> SUFFICES /\ TypeInv' /\ DomInv'
                 /\ \E i \in 0..L1+L2-1 : Psi4(i)'
                 /\ eip' = pm + 17
                 BY DEF Psi4
   <2> USE DEF I4
   <2> eip' = pm + 17 BY ConstAssumption, I4, eip = pm + 15
   <2>11 eip' \in Nat /\ eip' < pm + 25  BY ConstAssumption, pm \in Nat DEF I2
   <2>. eip' \in (0..MEMORY_END) 
     BY ConstAssumption, <2>11, pm + 25 < MEMORY_END, pm + 25 \in Nat DEF MEMORY_END             
    <2> TypeInv' BY <2>1, Phi2, ConstAssumption DEF I4, TypeInv            
    <2> eip' = pm + 17 BY ConstAssumption, I4, eip = pm + 15
    <2> \E d \in 0..L1+L2-1 : Psi4(d)' 
       <3> SUFFICES \E d \in (0..L1+L2-1) : 
           /\ ecx' = pA + sigma_it[d+1] 
           /\ \A j \in (0..d-1) : mem'[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]]
           /\ \A j \in (d+1..L1+L2-1) : mem'[pA+sigma_it[j]] = mem0[pA+sigma_it[j]]
           /\ al' = mem0[pA+sigma_it[d]]
           /\ d+1 <= L1+L2 - 1 => ah' = mem0[pA+sigma_it[d+1]] BY DEF Psi4
       <3> PICK ii \in (0..L1+L2-1) : Psi2(ii) BY DEF Phi2 
       <3> WITNESS ii \in (0..L1+L2-1)
       <3> /\ ecx' = pA + sigma_it[ii+1]
           /\ \A j \in (0..ii-1) : mem'[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]] 
           /\ \A j \in (ii+1..L1+L2-1) : mem'[pA+sigma_it[j]] = mem0[pA+sigma_it[j]]
           /\ al' = mem0[pA+sigma_it[ii]] 
        BY I4 DEF Psi2
       <3> ii+1 <= L1+L2 - 1 => ah' = mem0[pA+sigma_it[ii+1]] BY I4, ConstAssumption, Phi2
       <3> QED OBVIOUS
    <2> DomInv' BY <2>1, Phi2, ConstAssumption DEF I4, DomInv   
    <2>0 QED OBVIOUS
  <1>0 QED BY <1>1, <1>2, <1>3

 
 LEMMA F3 == Phi3 /\ Next => Phi4'
  <1> SUFFICES ASSUME Phi3, Next PROVE Phi4' OBVIOUS
  <1> USE DEF Phi3, Next, Phi4
  <1> I5 BY TypeInv, Next, L127
  <1> USE DEF I5
  <1> SUFFICES /\ TypeInv' /\ DomInv'
               /\ \E i \in (0..L1+L2) : Psi5(i)'
               /\ eip' = pm + 19 BY DEF Phi5
  <1> eip' = pm + 19 BY Phi3,  eip = pm + 17, ConstAssumption DEF I5
  <1> eip' \in (0..MEMORY_END) 
    BY ConstAssumption, eip' = pm + 19, pm + 25 < MEMORY_END DEF MEMORY_END
  <1>1 TypeInv' BY ConstAssumption, Phi3 DEF Phi3, TypeInv,  I5
  <1> DomInv' BY ConstAssumption, Phi3 DEF Phi3, TypeInv, DomInv, I5
  <1> SUFFICES \E i \in (0..L1+L2) : Psi5(i)' BY <1>1
  <1> PICK ii \in (0..L1+L2-1) : Psi4(ii) BY Phi3 DEF Psi4
  <1> ii + 1 \in (0..L1+L2) BY ConstAssumption
  <1> WITNESS ii + 1 \in (0..L1+L2)
  <1> SUFFICES /\ ecx' = pA + sigma_it[ii + 1]
               /\ \A j \in 0..(ii + 1) - 1 : mem'[pA + sigma_it[j + 1]] = mem0[pA + sigma_it[j]]
               /\ \A j \in ii + 1 + 1..L1 + L2 - 1 : mem'[pA + sigma_it[j]] = mem0[pA + sigma_it[j]]
               /\ ii + 1  =< L1 + L2 - 1 => ah' = mem0[pA + sigma_it[ii+1]] BY DEF Psi5, TypeInv, ConstAssumption
  <1>2 /\ ecx' = pA + sigma_it[ii+1]
       /\ ii + 1  =< L1 + L2 - 1 => ah' = mem0[pA + sigma_it[ii+1]]
      BY DEF Psi4, I5, TypeInv, ConstAssumption
  <1>3 \A j \in (0..ii) : mem'[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]]
    <2> TAKE k \in (0..ii)
    <2>1 k \in (0..ii-1) \/ k = ii BY ConstAssumption
    <2>2 CASE k \in (0..ii-1)
       <3>1 mem[pA+sigma_it[k+1]] = mem0[pA+sigma_it[k]] BY k \in (0..ii-1) DEF Psi4
       <3>2 k+1 # ii + 1 BY ConstAssumption, k \in (0..ii-1)
       <3> k + 1 \in (0..ii) BY ConstAssumption, <3>2
       <3>21 sigma_it[k+1] # sigma_it[ii+1] BY <3>2, TypeInv, ConstAssumption, SigmaIsInjective
       <3>22 sigma_it[k+1] \in (0..MEMORY_END) /\ sigma_it[ii+1] \in (0..MEMORY_END) BY ConstAssumption, k \in (0..ii-1), ii \in (0..L1+L2-1), stableSigmab
       <3>3 pA + sigma_it[k+1] # pA+sigma_it[ii+1] BY <3>22, <3>21, L13
       <3>4 ecx = pA+sigma_it[ii+1] BY Phi3 DEF Psi4 
       <3>5 pA + sigma_it[k+1] # ecx BY <3>3, <3>4, ConstAssumption
       <3>6 k+1 \in (0..L1+L2-1) BY ConstAssumption, k \in (0..ii-1), ii \in (0..L1+L2-1)
       <3>7 mem'[pA+sigma_it[k+1]] = mem[pA+sigma_it[k+1]] BY <3>5,<3>6, L8
       <3>8 mem'[pA+sigma_it[k+1]] =  mem0[pA+sigma_it[k]] BY <3>1, <3>7
       <3>0 QED BY <3>8
    <2>3 CASE k = ii
      <3>1 mem'[ecx] = al BY L9
      <3>2 ecx = pA + sigma_it[ii+1] BY <2>2 DEF Psi4
      <3>3 al = mem0[pA+sigma_it[ii]] BY <2>2 DEF Psi4
      <3>4 mem'[pA+sigma_it[ii+1]] = mem0[pA+sigma_it[ii]] BY <3>1, <3>2, <3>3
       <3>0 QED BY <3>4, k = ii
    <2>0 QED BY <2>1, <2>2, <2>3 
  <1>4 \A j \in (ii+2..L1+L2-1) : mem'[pA+sigma_it[j]] = mem0[pA+sigma_it[j]] 
    <2> TAKE k \in (ii+2..L1+L2-1)
    <2>1 k # ii+1 BY k \in (ii+2..L1+L2-1), ConstAssumption
    <2>21 sigma_it[k] # sigma_it[ii+1] BY <2>1, ConstAssumption, SigmaIsInjective
    <2>22 sigma_it[k] \in (0..MEMORY_END) /\ sigma_it[ii+1] \in (0..MEMORY_END) BY ConstAssumption, stableSigmab
    <2>2 pA + sigma_it[k] # pA + sigma_it[ii+1] BY <2>22, <2>21, (* ConstAssumption *) L13
    <2>3 ecx = pA+sigma_it[ii+1] BY Phi3 DEF Psi4 
    <2>4 pA + sigma_it[k] # ecx BY <2>2, <2>3
    <2>5 k \in (0..L1+L2-1) BY k \in (ii+2..L1+L2-1)
    <2>6 mem'[pA+sigma_it[k]] =  mem[pA+sigma_it[k]] BY <2>4, <2>5, L8
    <2>7 mem[pA+sigma_it[k]] = mem0[pA+sigma_it[k]] BY DEF Psi4
    <2>0 QED BY <2>6, <2>7
  <1>0 QED BY <1>2, <1>3, <1>4         

LEMMA F4 == Phi4 /\ Next => Phi5'
  <1> SUFFICES ASSUME Phi4, Next PROVE Phi5' OBVIOUS
  <1> USE DEF Phi4, Next, Phi5
  <1> I6 BY TypeInv, Next, L129
  <1> eip' = pm + 21 BY ConstAssumption DEF I6
  <1> eip' \in (0..MEMORY_END) BY ConstAssumption DEF MEMORY_END
  <1> TypeInv' BY Phi4, ConstAssumption DEF TypeInv, I6
  <1> DomInv' BY Phi4, ConstAssumption DEF TypeInv, DomInv, I6
  <1> SUFFICES \E d \in (0..L1+L2) : 
                    /\ ecx' = pA + sigma_it[d]
                    /\ \A j \in 0..d - 1 : mem'[pA + sigma_it[j + 1]] = mem0[pA + sigma_it[j]]
                    /\ \A j \in d + 1..L1 + L2 - 1 : mem'[pA + sigma_it[j]] = mem0[pA + sigma_it[j]]
                    /\ d =< L1 + L2 - 1 => al' = mem0[pA + sigma_it[d]]
                    BY DEF Phi5, TypeInv, Psi6
  <1>2 PICK ii \in (0..L1+L2) : Psi5(ii) BY Phi4    
  <1> ii \in (0..L1+L2) BY <1>2         
  <1>3 WITNESS ii \in (0..L1+L2)
  <1> SUFFICES  /\ ecx' = pA+sigma_it[ii] 
                /\ \A j \in (0..ii-1)     : mem'[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]]
                /\ \A j \in (ii+1..L1+L2-1) : mem'[pA+sigma_it[j]] = mem0[pA+sigma_it[j]]
                /\ ii =< L1 + L2 - 1 => al' = mem0[pA+sigma_it[ii]] OBVIOUS
  <1>4 /\ ecx' = pA+sigma_it[ii]
       /\ \A j \in (0..ii-1)     : mem'[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]] 
       /\ \A j \in (ii+1..L1+L2-1) : mem'[pA+sigma_it[j]] = mem0[pA+sigma_it[j]]
       /\ ii =< L1 + L2 - 1 =>  al' = mem0[pA+sigma_it[ii]] 
   BY <1>2, Phi4, ConstAssumption DEF I6, Psi5          
  <1>0 QED BY <1>3, <1>4
 
LEMMA F5 == Phi5 /\ Next => \/ /\ ecx = pA 
                               /\ Phi6'
                            \/ /\ ecx # pA
                               /\ Phi0'
  <1> SUFFICES ASSUME Phi5, Next  PROVE \/ /\ ecx = pA
                                           /\ Phi6'
                                        \/ /\ ecx # pA
                                           /\ Phi0'
    OBVIOUS
  <1> USE DEF Phi5, Next, Phi0, Phi6
  <1> I7 BY TypeInv, Next, L131
  <1>1 PICK ii \in (0..L1+L2) : Psi6(ii) BY Phi5
  <1>2 ii \in (0..L1+L2-1) \/ ii = L1+L2 BY L10
  <1>3 CASE ii = L1+L2
   <2>1 \A j \in (0..L1+L2-1) : mem[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]] BY <1>1, ii = L1+L2, ConstAssumption DEF Psi6
   <2>7 \A j \in 0..L1 + L2 - 1 : mem[pA + sigma[j]] = mem0[pA + j]
     BY <2>1, SigmaSwap 
   <2>2 \A j \in 0..L1 + L2 - 1 : mem'[pA + sigma[j]] = mem0[pA + j]
     BY <2>7 DEF I7   
   <2>4 ecx = pA+sigma_it[L1+L2] BY <1>1, ii = L1+L2, ConstAssumption DEF Psi6
   <2>5 ecx = pA BY sigma_itn, ConstAssumption, <2>4
   <2> \neg ecx # pA BY <2>5, ConstAssumption
   <2>6 eip' = eip + 4 BY <2>5 DEF I7
   <2>8 eip' = pm + 25 BY <2>6, eip = pm + 21, ConstAssumption DEF I7
   <2> eip' \in (0..MEMORY_END) BY ConstAssumption, <2>8, pm + 25 < MEMORY_END DEF MEMORY_END
   <2> TypeInv' BY ConstAssumption DEF Phi5, I7, TypeInv
   <2> DomInv' BY ConstAssumption DEF Phi5, I7, TypeInv, DomInv
   <2> SUFFICES Phi6' BY <2>5
   <2>0 QED BY <2>8, <2>2
  <1>4 CASE ii \in (0..L1+L2-1)
    <2> ii # L1+L2 /\ ii \in (0..L1+L2) /\ L1+L2 \in (0..L1+L2) BY ConstAssumption, ii \in (0..L1+L2-1)
    <2> sigma_it[ii] # sigma_it[L1+L2] BY SigmaIsInjective
    <2>1 sigma_it[ii] # 0 BY ConstAssumption, sigma_itn
    <2>2 sigma_it[ii] \in (0..L1+L2-1) /\ 0 \in (0..L1+L2-1) BY ii \in (0..L1+L2-1), Sigma_itType, ConstAssumption
    <2> pA+sigma_it[ii] # pA + 0 BY <2>2, <2>1, L13
    <2> pA + sigma_it[ii] # pA BY ConstAssumption
    <2> ecx # pA BY <1>1 DEF Psi6
    <2> ecx' # pA BY DEF I7
    <2>3 eip' = eip - 21 BY DEF I7
    <2> eip' \in (0..MEMORY_END) BY ConstAssumption, <2>3, pm + 25 < MEMORY_END DEF MEMORY_END
    <2> TypeInv' BY ConstAssumption DEF Phi5, I7, TypeInv
    <2> DomInv' BY ConstAssumption DEF Phi5, I7, TypeInv, DomInv
    <2>4 /\ ecx' = pA + sigma_it[ii] 
         /\  \A j \in 0..ii - 1 : mem'[pA + sigma_it[j + 1]] = mem0[pA + sigma_it[j]]
         /\  \A j \in ii + 1..L1 + L2 - 1 : mem'[pA + sigma_it[j]] = mem0[pA + sigma_it[j]]
      BY <1>1 DEF Psi6, I7 
    <2> SUFFICES Phi0' BY <2>3 DEF Phi0, Psi0, TypeInv
    <2> SUFFICES  \E i \in 0..L1 + L2 - 1 : Psi0(i)'
         BY <2>3, ConstAssumption, eip = pm + 21 DEF Phi0, I7    
    <2> WITNESS ii \in (0..L1+L2-1)
    <2> SUFFICES 
              /\ ecx' = pA + sigma_it[ii]
              /\ \A j \in 0..ii - 1 : mem'[pA + sigma_it[j + 1]] = mem0[pA + sigma_it[j]]
              /\ \A j \in ii + 1..L1 + L2 - 1 : mem'[pA + sigma_it[j]] = mem0[pA + sigma_it[j]]
              /\ al' = mem0[pA + sigma_it[ii]] BY DEF Psi0
    <2>5 /\ ecx' = pA+sigma_it[ii]
         /\ \A j \in 0..ii - 1 : mem'[pA + sigma_it[j + 1]] = mem0[pA + sigma_it[j]]
         /\ \A j \in ii + 1..L1 + L2 - 1 : mem'[pA + sigma_it[j]] = mem0[pA + sigma_it[j]]
      BY <1>1 DEF Psi0, Psi6, I7
    <2>6 al' = mem0[pA + sigma_it[ii]] 
     <3>1 ii <= L1+L2 - 1 BY ConstAssumption, ii \in (0..L1+L2-1)
     <3>2 al = mem0[pA + sigma_it[ii]] BY <3>1, <1>1, I7 DEF Psi6
     <3>0 QED BY <3>2 DEF I7
    <2>0 QED BY <2>5, <2>6
   
  <1>0 QED BY <1>2, <1>3, <1>4                              

LoopInv == Phi0 \/ Phi1 \/ Phi2 \/ Phi3 \/ Phi4 \/ Phi5 \/ Phi6

LEMMA F6 == Phi6 /\ Next => LoopInv'
  <2> USE DEF Phi6, TypeInv, Next, DomInv
  <2> SUFFICES ASSUME Phi6, Next PROVE LoopInv' OBVIOUS
  <2> eip = pm + 25 BY DEF Phi6
  <2>1 mem[eip + 0] = 2 BY StableCode, ConstAssumption, InitMemoryAssumption
  <2>  \neg (I0 \/ I1 \/ I2 \/ I3 \/ I4 \/ I5 \/ I6 \/ I7)  
  BY <2>1 DEF Phi6, Next, I0, I1, I2, I3, I4, I5, I6, I7
  <2>0 QED OBVIOUS

LEMMA Linit == Init => LoopInv
  <1> SUFFICES ASSUME Init PROVE Phi0 /\ eip = pm BY DEF LoopInv
  <1> USE DEF Init, Phi0
  <1> SUFFICES /\ TypeInv /\ DomInv /\ \E i \in (0..L1+L2-1) : Psi0(i) BY DEF Phi0
  <1>1 TypeInv BY ConstAssumption DEF TypeInv
  <1>2 DomInv BY ConstAssumption DEF DomInv
  <1> SUFFICES \E i \in (0..L1+L2-1) : Psi0(i)  BY <1>1, <1>2
  <1> 0 \in (0..L1+L2-1) BY ConstAssumption
  <1> WITNESS 0 \in (0..L1+L2-1) 
  <1> SUFFICES /\ pA+sigma_it[0] = ecx
               /\ \A j \in (0..0-1) : mem[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]]
               /\ \A j \in (0..L1+L2-1) : mem[pA+sigma_it[j]] = mem0[pA+sigma_it[j]]   
         BY ConstAssumption DEF Psi0      
   <1>3 pA+sigma_it[0] = ecx BY sigma_it[0] = 0, ConstAssumption  DEF Init         
   <1>4 \A j \in (0..0-1) : mem[pA+sigma_it[j+1]] = mem0[pA+sigma_it[j]] 
     OBVIOUS
   <1>5 \A j \in (0..L1+L2-1) : mem[pA+sigma_it[j]] = mem0[pA+sigma_it[j]] 
     BY ConstAssumption DEF Init                             
  <1>0 QED BY ConstAssumption, <1>3, <1>4, <1>5 DEF Phi0, Init
 
LEMMA LNext == LoopInv /\ [Next]_<<al, ah, ecx, mem, eip>> => LoopInv'                                 
   <1> SUFFICES ASSUME LoopInv, [Next]_<<al, ah, ecx, mem, eip>> 
       PROVE LoopInv' OBVIOUS
   <1> USE DEF Next, LoopInv 
   <1> Phi0 \/ Phi1 \/ Phi2 \/ Phi3 \/ Phi4 \/ Phi5 \/ Phi6 \/ UNCHANGED <<al, ah, ecx, mem, eip>> 
   BY DEF LoopInv
   <1>1 CASE UNCHANGED <<al, ah, ecx, mem, eip>>
    <2> SUFFICES LoopInv' OBVIOUS
    <2> USE DEF LoopInv
    <2> SUFFICES   /\ Phi0 => LoopInv'
                   /\ Phi1 => LoopInv'
                   /\ Phi2 => LoopInv'
                   /\ Phi3 => LoopInv'
                   /\ Phi4 => LoopInv'
                   /\ Phi5 => LoopInv'
                   /\ Phi6 => LoopInv'
        OBVIOUS   
    <2>0 QED BY ConstAssumption, UNCHANGED <<al, ah, ecx, mem, eip>>  DEF Phi0, Phi1, Phi2, Phi3, Phi4, Phi5, Phi6, 
                                    TypeInv, DomInv, Psi0, Psi2, Psi4, Psi5, Psi6 
   <1>2 CASE Next
     <2>0 SUFFICES /\ Phi0 => LoopInv'
                   /\ Phi1 => LoopInv'
                   /\ Phi2 => LoopInv'
                   /\ Phi3 => LoopInv'
                   /\ Phi4 => LoopInv'
                   /\ Phi5 => LoopInv'
                   /\ Phi6 => LoopInv'
        OBVIOUS
     <2> CASE Phi0 BY F0, Next  
     <2> CASE Phi1 BY F1, Next 
     <2> CASE Phi2 BY F2, Next
     <2> CASE Phi3 BY F3, Next 
     <2> CASE Phi4 BY F4, Next
     <2> CASE Phi5 BY F5, Next
     <2> CASE Phi6 BY F6, Next
     <2>1 QED BY F0, F1, F2, F3, F4, F5, F6, Next                
   <1> QED BY  <1>1, <1>2                           
                                
THEOREM PermutationCorrection == Init /\ [][Next]_<<al, ah, ecx,  mem, eip>> => [] LoopInv
BY Linit, LNext, PTL                               
                                                                             
=============================================================================
\* Modification History
\* Last modified Fri Jan 15 16:15:31 CET 2016 by bonfante
\* Created Tue Dec 22 10:26:38 CET 2015 by bonfante
